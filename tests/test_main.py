from main import *


def test_exercise_1():
    result = exercise_1(5)
    expected = [0, 1, 2, 3, 4]

    assert type(result) == list, \
        'Verifique se a função exercise_1 está retornando uma LISTA'

    assert result == expected, \
        'Verifique se a função exercise_1 está retornando no intervalo correto'


def test_exercise_1_yield():
    result = exercise_1_yield(5)
    expected = [0, 1, 2, 3, 4]

    for index, number in enumerate(result):
        assert number == expected[index], \
            'Verifique se a função está retornando um generator YIELD corretamente'
